# Leafony STM32 MCU のカスタムボード設定の課題

Arduino IDEでSTM32 MCUリーフのスケッチを書くときに端子名の課題があります。
同じMCUをつかうNUCLEO-L452REボードの設定を使用していますが、Leafonyバスの都合で7個の端子割当てが異なっています。その為スケッチにて、Arduinoの端子名(D1）ではなくSTM32 MCUのピン名(例 PA2)を使う必要があります。カスタムボード設定により端子名の並び替えを試行してみました。

## Leafonyの情報

* [AP03 STM32 MCU](https://docs.leafony.com/docs/leaf/processor/ap03/) 
* [開発環境設定](https://docs.leafony.com/docs/environment/) : ”STM32 MCU 開発環境”の項

## 参考にした情報

* 評価ボードの情報は variants というフォルダーにあります。
* カスタムボード設定の方法についての[ブログ](https://www.disk91.com/2018/technology/hardware/stm32-and-arduino-working-with-a-custom-board/) を参考にしました。
* このブログによると、/Document/Arduino/hardware/stm32/v.x.y/ ディレクトリに配置するとの事。

## 設定方法の概要

* Arduino IDEのインストールに際して「Portable」フォルダーの使用が推奨されていました。
* Windows10になって「ArduinoData」フォルダーが使用されており、スケッチを収める「Arduino」フォルダーの「hardware」サブフォルダーが未使用である事に気が付きました。ですから、ここにカスタムボード設定を置けば良さそうです。
* 手順としては、\ArduinoData\packages\STM32\hardware\stm32\1.9.0から
　 boards.txt とplatform.txt をコピーして、後者のタイトル行を修正。
* 上記の「variants」サブフォルダーにある NUCLEO_L452REサブフォルダーをコピーして、
  その中にある variant.h  variant.cpp のピン割当を編集する。

![ボードマネージャのフォルダー](images/board-manager.png)

## 設定の詳細

### A. platform.txt と boards.txt の設定

* ~\ArduinoData\packages\STM32\hardware\stm32\1.9.0に
   platform.txt と boards.txt をコピーする
* Platfrom.txt ファイルの 7行目のボード名称だけを編集する 
   name=STM32 Custom Boards (selected from submenu) 
* boards.txtを編集する
  * 多数あるボード記述を２～３個に減らして見る
  * NUCLEO_L452REサブフォルダーを含める
  * 上記をコピーしてLeafony用に名称等を変更する

### B. ロジカルリンク

* ~\ArduinoData\packages\STM32\hardware\stm32\1.9.0の同名のフォルダーである
　 cores, libraries, systemフォルダーへのロジカルリンクを張る
* Windows10の場合はジャンクションが使える
  * 引用したブログはMacPCの事例です

![フォルダの図](images/folders.png)

### C. variantファイルの作成

* ~\ArduinoData\packages\STM32\hardware\stm32\1.9.0\variantsのフォルダーにある
   NUCLEO_L452REサブフォルダーをコピーして編集する
* Leafony用にフォルダー名を変更する
  * このフォルダーに variant.h と variant.cpp の２つのファイルがあり、そのピン設定を変更する
  * ピン設定の変更は、これら両方の記述を変更する

![IDEのメニュー表示](images/Arduino-IDE.png)

### D. 評価状況 (as of 2021/2/26)

* [x] D6~D11の動作を[AI03 MIC&VR&LED](https://docs.leafony.com/docs/leaf/io/ai03/)にて確認
* [ ] A1/A2でUARTの動作確認は未だ
* [ ] D8/D9でUARTの動作確認は未だ
* [ ] SWDは未検討
* [ ] コンパイラの警告がでる

### E. 問題

現状、コンパイラで警告がでます。
その１：
  警告：ライブラリSrcWrapperはアーキテクチャstm32に対応したものであり、アーキテクチャ1.9.0で動作するこのボードとは互換性がないかもしれません。
その２：
　\<command-line>: warning: ISO C++11 requires whitespace after the macro name
　\<command-line>: warning: ISO C99 requires whitespace after the macro name

### F. 付記

stm32duino の Arduino Core の ver2.0.0 のリリースが予定されています。
variants 追加方法の変更が[予告](https://github.com/stm32duino/wiki/wiki/Add-a-new-variant-%28board%29)されています。
